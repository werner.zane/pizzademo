package com.example.glidetest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.CheckBox
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.bumptech.glide.Glide


class MainActivity : AppCompatActivity() , OnClickListener {
    //image and checkboxes
    private var checkBox: CheckBox? = null
    private var checkBoxTwo: CheckBox? = null
    private var checkBoxThree: CheckBox? = null
    private var checkBoxFour: CheckBox? = null

    private var imageIdList: List<Int> = listOf(
        R.drawable.cheese_pizza,
        R.drawable.black_olives_removebg_preview,
        R.drawable.pepperoni_removebg_preview,
        R.drawable.mushrooms_removebg_preview,
        R.drawable.peppers_removebg_preview
    )

    private var imageViewsList: ArrayList<ImageView> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        checkBox = findViewById(R.id.checkBox)
        checkBoxTwo = findViewById(R.id.checkBox2)
        checkBoxThree = findViewById(R.id.checkBox3)
        checkBoxFour = findViewById(R.id.checkBox4)

        checkBox?.setOnClickListener(this)
        checkBoxTwo?.setOnClickListener(this)
        checkBoxThree?.setOnClickListener(this)
        checkBoxFour?.setOnClickListener(this)

        createImageViews()

        showPizza()


    }


    fun createImageViews() {
        //go through image id
        val imageHolder = findViewById<ConstraintLayout>(R.id.imageHolder)
        imageIdList.forEach {
            val view = ImageView(this.applicationContext)

            val set = ConstraintSet().also { it.clone(imageHolder) }
//create image views
            with(view) {
                id = View.generateViewId()

                imageHolder.addView(this)
            }

            with(set) {
                connect(
                    view.id,
                    ConstraintSet.TOP,
                    imageHolder?.id ?: -1,
                    ConstraintSet.TOP,
                    0
                )
                connect(
                    view.id,
                    ConstraintSet.START,
                    imageHolder?.id ?: -1,
                    ConstraintSet.START,
                    0
                )
                connect(
                    view.id,
                    ConstraintSet.END,
                    imageHolder?.id ?: -1,
                    ConstraintSet.END,
                    0
                )
                connect(
                    view.id,
                    ConstraintSet.BOTTOM,
                    imageHolder?.id ?: -1,
                    ConstraintSet.BOTTOM,
                    0
                )
                applyTo(imageHolder)
            }
            imageHolder?.getViewById(view.id)?.visibility = View.INVISIBLE
            //add image views to imageview list
            imageViewsList.add(view)

        }


        //onclick
    }


    fun showPizza() {
        Glide.with(this)
            .load(imageIdList[0])
            .into(imageViewsList[0])

        imageViewsList[0].visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {


        fun olives() {
            Glide.with(this)
                .load(imageIdList[1])
                .into(imageViewsList[1])

            imageViewsList[1].visibility = View.VISIBLE
        }

        fun pepperoni() {
            Glide.with(this)
                .load(imageIdList[2])
                .into(imageViewsList[2])

            imageViewsList[2].visibility = View.VISIBLE
        }

        fun mushrooms() {
            Glide.with(this)
                .load(imageIdList[3])
                .into(imageViewsList[3])

            imageViewsList[3].visibility = View.VISIBLE
        }

        fun peppers() {
            Glide.with(this)
                .load(imageIdList[4])
                .into(imageViewsList[4])

            imageViewsList[4].visibility = View.VISIBLE
        }



        if (checkBox?.isChecked!!) {
            olives()
        } else {
            imageViewsList[1].visibility = View.INVISIBLE
        }
        if (checkBoxTwo?.isChecked!!) {
            pepperoni()
        } else {
            imageViewsList[2].visibility = View.INVISIBLE
        }
        if (checkBoxThree?.isChecked!!) {
            mushrooms()
        } else {
            imageViewsList[3].visibility = View.INVISIBLE
        }

        if (checkBoxFour?.isChecked!!) {
            peppers()
        } else {
            imageViewsList[4].visibility = View.INVISIBLE
        }

    }
}





